bucket-wiki
===========

bucket-wiki is a browser-based creole wiki viewer and wysiwyg editor.  It can be used fully standalone offline for creating and viewing a personal file-based wiki.

The purpose for which bucket-wiki was created is to complement the bitbucket wiki section when using git.

As described in a new/empty wiki on bitbucket, you can checkout your bitbucket wiki as a git repository.  Edit it offline (now with the bucket-wiki wysiwyg editor).  And push changes from your git repo back to origin (bitbucket).  This method lets you have your code and your wiki all available to you for reference and commits even when you are working offline (great for travel and temporary worksites).


License
-------
Copyright (c) 2012, Kevin Walton.

MIT License:

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.



Included libraries that are unmodified remain under the terms of their original licenses.

The following open source projects are utilized by bucket-wiki:
  - JSCreoleWiki http://code.google.com/p/jscreolewiki/	(modified under MIT)
  
  - jscreole  http://github.com/dwalters/jscreole (unmodified)
  - CKEditor	http://ckeditor.com/license  (unmodified MPLv2)
  - CKEditor plugins from LifeRay Portal https://github.com/liferay/liferay-portal  (unmodified GPLv3)
