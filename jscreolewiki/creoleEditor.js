
CreoleEditor = function( input  ){
    if( !input ){
        return null;
    }

    var editor = {};

    var ta = false;
    var ck = true;

    /*
    var itype = typeof input;
    if( itype == typeof HTMLTextAreaElement ){
        ta = true;
    }else if( itype == typeof CKEDITOR.editor ){
        ck = true;
    }
    */

    function setCreole( creole ){
        if( ta ){
            input.value = creole;
        }else if( ck ){
            input.setData( creole );
        }
    }

    function getCreole( creole ){
        if( ta ){
            return input.value;
        }else if( ck ){
            return input.getData();
        }
    }

    function setKeyUpHandler( site ){

        if( ck ){
            input.on('contentDom', function() {
                input.document.on('keyup', function( e ){
                    site.render( getCreole() );
                    //callback(e);
                });
            });
        }
    }

    function reset(){
        if( ck ){
            input.setData( '' );
            input.resetUndo();
        }
    }

    function setPath( path ){
        //alert(path);
        CKEDITOR.config.baseHref = path;
    }


    editor.reset = reset;
    editor.setCreole = setCreole;
    editor.getCreole = getCreole;
    editor.setKeyUpHandler = setKeyUpHandler;
    editor.setPath = setPath;

    return editor;
};