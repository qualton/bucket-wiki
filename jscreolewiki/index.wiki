[[http://code.google.com/p/jscreolewiki/|JsCreole Wiki]] is an offline wiki program run in a browser, it is based on jscreole, jQuery and other open source code.

= Features
== Offline Wiki
Can be edited and browse offline, also support publish online without modify, have no built in version control support.
== Multi sub site support
Every sub folder can be a sub site(due to firefox security constraint, the wiki script can only access current folder or sub folders)
== Site config
With built in site config support, include menu config, try [[wikiConfig]], or use link on rigth corner.
== [[http://ajaxpatterns.org/Unique_URLs|Unique_URLs]]
Every wiki can be bookmarked.
== Plan
See on [[Roadmap]]

Any question, please contact liuyehui at gmail dot com