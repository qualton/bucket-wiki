[[WikiCreole:Creole1.0|{{http://www.wikicreole.org/attach/LeftMenu/viki.png|Creole 1.0}}]]\\
What you see is a **live** demonstration of [[WikiCreole:Creole1.0|Creole 1.0]] parser, written entirely in [[Wikipedia:JavaScript|JavaScript]]. Creole is a wiki markup language, intended to be a cross standard for various wiki markup dialects.

* Modifying the text in the textarea below results in instant rendering XHTML without page reload.
* Try it //right now//!

See [[WikiCreole:CheatSheet|Creole 1.0 Cheat Sheet]] for editing tips.

The content is from [[http://www.ivan.fomichev.name/2008/04/javascript-creole-10-wiki-markup-parser.html|Codeholic's Codex: JavaScript Creole 1.0 Wiki Markup Parser]]